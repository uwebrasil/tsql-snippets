USE [a30Sigpo]
GO
/****** Object:  StoredProcedure [dbo].[PIVOT_A30PRODUTO2]    Script Date: 04/07/2014 16:26:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<UweKristmann>
-- Create date: <16.11.2013>
-- Description:	<Pivot a table, rows to columns>
--              done without the PIVOT-Function
--              which needs compatibility level
--              set to 90.     
-- =============================================
ALTER PROCEDURE [dbo].[PIVOT_A30PRODUTO2]
	-- Add the parameters for the stored procedure here
	@p_produto varchar(50),
	@p_grupo   varchar(50),
	@p_CODIGO  varchar(6),
	@p_posto   varchar(MAX)
AS
BEGIN
    --TRUNCATE TABLE a30sigpo.dbo.A10LOG
    
    --insert into a30sigpo.dbo.A10LOG(vc) values(@p_produto)
    --insert into a30sigpo.dbo.A10LOG(vc) values(@p_grupo)
    --insert into a30sigpo.dbo.A10LOG(vc) values(@p_posto)
    
	DECLARE @sql nvarchar(MAX)
	DECLARE @cols AS NVARCHAR(MAX)
    SET @cols = ''

    DECLARE @posto TABLE 
    (
     ID INT IDENTITY(1, 1) primary key,
     posto varchar(3)
    )

    INSERT INTO @posto(posto) SELECT distinct posto FROM a30sigpo.dbo.a30produto
    
    DECLARE db_cursor CURSOR FOR SELECT posto FROM @posto 
    DECLARE @xposto varchar(3)
    DECLARE @xid int
    
    OPEN db_cursor  
    FETCH NEXT FROM db_cursor INTO @xposto 
    WHILE @@FETCH_STATUS = 0  
      BEGIN
        -- posto selecionados, 999 -> todos os postos 
        IF (charindex('9988', @p_posto) > 0) OR (charindex(@xposto, @p_posto) > 0)
         BEGIN
           SET @cols = @cols + 'MAX(CASE WHEN posto = '''+@xposto
                             +''' THEN preco ELSE NULL END) ['+@xposto+'],'      
         END                 
          
        FETCH NEXT FROM db_cursor INTO @xposto  
      END  

    CLOSE db_cursor  
    DEALLOCATE db_cursor 
 
    SET @cols = SUBSTRING(@cols,0,len(@cols))   
     
    declare @central varchar(3)
    select @central=central from A30PARAMETRO    
    SET @sql = 'select a.produto, b.descricao from 
                a30produto a, 
               ( select produto, descricao, posto from a30produto 
                 where 1=1
                   and Posto = coalesce('+ @central +',posto)     
               ) b
              where a.Produto = b.produto'  
    --IF len(@cols) > 0
    -- BEGIN
    --  SET @sql = 'select produto,descricao,' + @cols + ' from a30sigpo.dbo.a30produto where 1=1 '
    -- END
    --ELSE
    -- -- nenhum posto selecionado
    -- BEGIN
    --   SET @sql = 'select produto,descricao' + @cols + ' from a30sigpo.dbo.a30produto where 1=1 '
    -- END  
    
    -- filtro produto
    IF LEN(@p_produto) > 0
     BEGIN
      SET @sql = @sql + ' AND b.descricao LIKE ''%'+@p_produto+'%'' '
     END    
    -- filtro Codigo
    IF LEN(@p_codigo) > 0
     BEGIN
      SET @sql = @sql + ' AND a.produto LIKE ''%'+@p_codigo+'%'' '
     END    
    -- filtro grupo  
    IF LEN(@p_grupo) > 0
     BEGIN
      SET @sql = @sql 
               + ' AND b.descricao IN ( select descricao from A30PRODUTO
                                      where grupo IN ( select Grupo from A30GRUPO
                                                       where descricao LIKE ''%'+@p_grupo+'%'')) '
      END    
                                            
    SET @sql = @sql + ' group by a.produto,b.descricao order by a.produto' 
    
    --insert into a30sigpo.dbo.A10LOG(vc) values(@sql)        
    EXEC sp_executesql @sql 
    
END 