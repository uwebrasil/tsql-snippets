-- =============================================
-- Author:		Uwe Kristmann
-- Create date: 09.04.2014
-- Description:	Retorna os produtos que ainda podem 
--              ser replicados pelos todos os postos
--              da tabela A30POSTOS
-- =============================================
CREATE PROCEDURE Analise_Replica_Produto
	-- Add the parameters for the stored procedure here
	@p_tabela   varchar(50),
	@p_all      varchar(1) = NULL
AS
BEGIN
    DECLARE @sql nvarchar(MAX)
    
    IF OBJECT_ID('tempdb..#tproduto') IS NOT NULL DROP TABLE #tproduto
    CREATE TABLE #tproduto ( produto varchar(6) not null,
                             descricao varchar(50), 
                             tabela varchar(50))
    SET @sql = '
    INSERT INTO  #tproduto(produto,tabela)
     select produto,'''+@p_tabela+''' from 
     (
      select a.produto,COUNT(distinct a.posto) cnt from '+@p_tabela+' a 
       where 1=1
         and exists (select Posto from '+@p_tabela+' 
                      where Produto = a.produto 
                      and Posto = (select central from A30PARAMETRO))              
      group by a.produto
     ) aliasA 
     where cnt != (select COUNT(distinct codigo) from A30POSTOS);
     '
   -- acrescentar descricao
   SET @sql = @sql + '
   UPDATE #tproduto 
    set descricao = x.descricao
    from #tproduto t inner join A30PRODUTO x 
                     on t.produto = x.produto
     where x.Posto = (select central from A30PARAMETRO); 
    '
    IF @p_all IS NOT NULL 
     SET @sql = @sql +
      'select produto +'' (''+coalesce(descricao,''sem descricao'')+'')'',tabela from #tproduto'
    ELSE
     SET @sql = @sql +
      'select produto +'' (''+coalesce(descricao,''sem descricao'')+'')'' from #tproduto 
       group by produto +'' (''+coalesce(descricao,''sem descricao'')+'')'''
	   
	EXEC sp_executesql @sql 
	
END