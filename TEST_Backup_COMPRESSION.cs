﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Microsoft.SqlServer.Management.Smo;
//using Microsoft.SqlServer.ConnectionInfo;
//using Microsoft.SqlServer.Smo;
//using Microsoft.SqlServer.SmoEnum.dll;
//using Microsoft.SqlServer.SmoExtended;
//using Microsoft.SqlServer.SqlEnum;
using Microsoft.SqlServer.Management.Sdk.Sfc;
using Microsoft.SqlServer.Management.Common;
using System.Configuration;
using System.Data.SqlClient;

namespace myBackup
{
    public partial class Form1 : Form
    {
        //Server myServer = new Server(@"ARSHADALI-LAP\ARSHADALI");
        Server myServer = new Server(@"UWE-PC\SQLEXPRESS");
        //Database myDatabase = myServer.Databases["OsExpress"];
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            doBackup2();
        }

        private void doBackup()
        {
            Database myDatabase = myServer.Databases["OsExpress"];
            Backup bkpDBFullWithCompression = new Backup();
            /* Specify whether you want to back up database or files or log */
            bkpDBFullWithCompression.Action = BackupActionType.Database;
            /* Specify the name of the database to back up */
            bkpDBFullWithCompression.Database = myDatabase.Name;
            /* You can use back up compression technique of SQL Server 2008,
             * specify CompressionOption property to On for compressed backup */

            bkpDBFullWithCompression.CompressionOption = BackupCompressionOptions.On;

            bkpDBFullWithCompression.Devices.AddDevice(@"C:\UWE\OsExpress.bak", DeviceType.File);
            //bkpDBFullWithCompression.Devices.AddDevice("\\\\uwe-pc\\UWE\\OsExpress.bak2", DeviceType.File);
            bkpDBFullWithCompression.BackupSetName = "OSExpress database Backup - Compressed";
            bkpDBFullWithCompression.BackupSetDescription = "OSExpress database - Full Backup with Compressing - only in SQL Server 2008";
            bkpDBFullWithCompression.SqlBackup(myServer);
        }

        private void doBackup2()
        {
		//SELECT SERVERPROPERTY('productversion'), SERVERPROPERTY ('productlevel'), SERVERPROPERTY ('edition')
            // read connectionstring from config file
            var connectionString = ConfigurationManager.ConnectionStrings["Backup.Properties.Settings.OsExpressConnectionString"].ConnectionString;

            // read backup folder from config file ("C:/temp/")
            //var backupFolder = ConfigurationManager.AppSettings["BackupFolder"];
            var backupFolder = "C:\\Uwe\\";

            var sqlConStrBuilder = new SqlConnectionStringBuilder(connectionString);

            // set backupfilename (you will get something like: "C:/temp/MyDatabase-2013-12-07.bak")
            var backupFileName = String.Format("{0}{1}-{2}.bak",
                backupFolder, sqlConStrBuilder.InitialCatalog,
                DateTime.Now.ToString("yyyy-MM-dd"));

            using (var connection = new SqlConnection(sqlConStrBuilder.ConnectionString))
            {
                var query = String.Format("BACKUP DATABASE {0} TO DISK='{1}' WITH COMPRESSION",
                    sqlConStrBuilder.InitialCatalog, backupFileName);

                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
